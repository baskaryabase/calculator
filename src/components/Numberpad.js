import React from "react";
import { Button, Row, Col } from 'reactstrap';

export default function NumberPad(props) {
    function handleClick(e) {
        props.buttonClicked(e.target.name)
    }
    return (
        <React.Fragment>
            {[1, 2, 3, 4, 5, 6, 7, 8, 9, 0].map((val, i) =>
                <Col key={val} xs="3">
                    <Button className="button" name={val} onClick={handleClick}>{val}</Button></Col>
            )}
            {["+", "-", "*", "/"].map((val, i) =>
                <Col key={val} xs="3">
                    <Button className="button" name={val} onClick={handleClick}>{val}</Button></Col>

            )}
            <Col xs="3"><Button className="button" name="=" onClick={handleClick}>=</Button></Col>
            <Col xs="3"><Button className="button" name="CLEAR" onClick={handleClick}>CE</Button></Col>
            <Col xs="3"><Button className="button" name="DEL" onClick={handleClick}>DEL</Button></Col>

        </React.Fragment>
    )
}