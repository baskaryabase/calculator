import React from "react";

class ChildComponent extends React.Component {
    render() {
        console.log(this.props.value)
        return (
            <h1>HI {this.props.value}</h1>
        )
    }
}

class Display extends React.Component {
    state = {
        displayVal: 0
    }
    static getDerivedStateFromProps(props, state) {
        console.log(props)
        return props;
    }

    componentDidMount() {
        console.log("Display mounted")
    }
    componentDidUpdate() {
        console.log("Display updated")
    }
    componentWillUnmount() {
        console.log("Display i am gonna unmounted");
    }
    shouldComponentUpdate(nextProps, nextState) {
        return true
    }
    render() {
        return <React.Fragment>
            <div className="display">
                <h1 >{this.state.displayVal || 0}</h1>
            </div>
            <ChildComponent value={this.state.displayVal} />
        </React.Fragment>
    }
}

export default Display;