import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Display from "./components/Display"
import NumberPad from "./components/Numberpad"
import { Container, Row, Col } from "reactstrap"
let operators = ["+", "-", "*", "/"]
let operations = {
  "add": function (val1, val2) { return val1 + val2; },
  "sub": function (val1, val2) { return val1 - val2; },
}
function App() {
  const [value, setValue] = useState("")
  const [mount, setMount] = useState(true)

  function buttonClicked(val) {
    try {
      if (val === "CLEAR") {
        return setValue("")
      }
      if (val === "DEL") {
        return setValue(preValue => preValue.slice(0, preValue.length - 1))
      }
      if (val === "=") {
        let newVal = eval(value);
        return setValue(newVal);
      }
      console.log(value[value.length - 1], val)
      setValue(preValue => `${preValue}${val}`)
    } catch (err) {
      console.log(err)
      return setValue("error")
    }
  }
  return (
    <Container style={{ marginTop: window.innerHeight / 3, padding: "auto", maxWidth: 400 }}>
      <Row><Display
        displayVal={value}
      /></Row>

      <Row> <NumberPad buttonClicked={buttonClicked} /></Row>
    </Container>
  );
}

export default App;
